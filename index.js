const userAge = +prompt('Введіть свій вік:');

if (userAge){
    if (userAge > 0 && userAge <= 90){
        if (userAge < 12){
            alert('Ти дитина');
        }else if(userAge < 18){
            alert('Ти підліток');
        }else if(userAge >= 18){
            alert('Ви є доросла людина');
        }
    }else {
        alert('Вік не відповідає дійсності');
    }
}else{
    alert('Ви ввели не число');
}

const userMonth = prompt('Введіть потрібний вам місяць з маленької літери:')
switch (userMonth){
    case 'січень' : console.log('31 днів в цьому місяці'); break;
    case 'лютий' : console.log('28 днів в цьому місяці'); break;
    case 'березень' : console.log('31 днів в цьому місяці'); break;
    case 'квітень' : console.log('30 днів в цьому місяці'); break;
    case 'травень' : console.log('31 днів в цьому місяці'); break;
    case 'червень' : console.log('30 днів в цьому місяці'); break;
    case 'липень' : console.log('31 днів в цьому місяці'); break;
    case 'серпень' : console.log('31 днів в цьому місяці'); break;
    case 'вересень' : console.log('30 днів в цьому місяці'); break;
    case 'жовтень' : console.log('31 днів в цьому місяці'); break;
    case 'листопад' : console.log('30 днів в цьому місяці'); break;
    case 'грудень' : console.log('31 днів в цьому місяці'); break;
    default: console.log('Такого місяця не існує')
}